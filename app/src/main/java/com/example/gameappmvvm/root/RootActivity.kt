package com.example.gameappmvvm.root

import android.os.Bundle
import com.example.gameappmvvm.R
import com.example.gameappmvvm.common.BaseActivity

class RootActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_root)
    }
}