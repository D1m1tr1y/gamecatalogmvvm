package com.example.gameappmvvm.common.di

import androidx.room.Room
import androidx.room.RoomDatabase
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object NetworkModule : InjectionModule {
    private const val BASE_URL = "https://api.rawg.io/api/"
    override fun create() = module {
        single {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build()
            Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .client(client)
                .build()
        }
        single{
            Room.databaseBuilder(get(), RoomDatabase::class.java, "Rick and Morty")
                .build()
        }
    }
}