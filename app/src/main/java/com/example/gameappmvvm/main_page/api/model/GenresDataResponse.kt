package com.example.gameappmvvm.main_page.api.model

import com.google.gson.annotations.SerializedName

data class GenresDataResponse(
    @SerializedName("count")
    val count : Int,
    @SerializedName("next")
    val next : String?,
    @SerializedName("previous")
    val previous : String?,
    @SerializedName("results")
    val results : List<ResultsResponse>
)
