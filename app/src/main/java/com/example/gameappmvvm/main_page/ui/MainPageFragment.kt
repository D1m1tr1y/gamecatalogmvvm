package com.example.gameappmvvm.main_page.ui

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.gameappmvvm.R
import com.example.gameappmvvm.common.BaseFragment
import com.example.gameappmvvm.databinding.FragmentMainpageBinding
import com.example.gameappmvvm.main_page.ui.adapter_genres_recyclerview.GenresAdapter
import com.example.gameappmvvm.utils.viewbinding.viewBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainPageFragment : BaseFragment(R.layout.fragment_mainpage) {
    private val viewModel: MainPageViewModel by viewModel()
    private val binding: FragmentMainpageBinding by viewBinding()
    private val adapter: GenresAdapter by lazy {
        GenresAdapter()
    }
    private val layoutManager: LinearLayoutManager by lazy {
        LinearLayoutManager(context)
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            genreRecyclerView.layoutManager = layoutManager
            genreRecyclerView.setHasFixedSize(true)
            genreRecyclerView.adapter = adapter
            adapter.onAttachedToRecyclerView(genreRecyclerView)
        }
        viewModel.getGenres()
        viewModel.upsertGenres()
        observe(viewModel.genresData) {
            adapter.setData(it)
        }
    }
}