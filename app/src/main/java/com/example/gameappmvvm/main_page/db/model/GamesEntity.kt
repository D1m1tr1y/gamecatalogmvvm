package com.example.gameappmvvm.main_page.db.model

data class GamesEntity(
    val id : Int,
    val name : String,
)
