package com.example.gameappmvvm.main_page.model

import com.google.gson.annotations.SerializedName

data class GenresData(
    @SerializedName("count")
    val count : Int,
    @SerializedName("next")
    val next : String?,
    @SerializedName("previous")
    val previous : String?,
    @SerializedName("results")
    val results : List<Results>
)
