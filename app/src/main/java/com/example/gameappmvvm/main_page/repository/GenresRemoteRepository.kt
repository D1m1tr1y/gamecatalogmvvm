package com.example.gameappmvvm.main_page.repository
import com.example.gameappmvvm.main_page.api.GenresApi
import com.example.gameappmvvm.main_page.model.GenresData
import com.example.gameappmvvm.main_page.model.GenresDataResponseConverter

class GenresRemoteRepository(
  private val api : GenresApi
) : RemoteRepository {
    private val key = "6a6e1f2a3cec4a488bb812428dfb4a70"
    override suspend fun getData(): GenresData =
        GenresDataResponseConverter.fromNetwork(
            api.getData(key)
        )
}
