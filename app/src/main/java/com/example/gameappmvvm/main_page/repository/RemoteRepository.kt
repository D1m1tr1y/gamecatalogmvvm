package com.example.gameappmvvm.main_page.repository

import com.example.gameappmvvm.main_page.model.GenresData

interface RemoteRepository {
    suspend fun getData(): GenresData
}