package com.example.gameappmvvm.main_page.ui.model

import com.example.gameappmvvm.main_page.model.Games
import com.example.gameappmvvm.main_page.model.GenresData

object GenresUiConverter {
    fun fromNetwork(response : GenresData): List<ResultsUi> =
        response.results.map { results ->
            ResultsUi(
                id = results.id,
                name = results.name,
                gamesCount = results.gamesCount,
                image = results.image,
                games = fromNetworkGames(results.games)
            )
        }
    private fun fromNetworkGames(response : List<Games>): List<GamesUi> =
        response.map{results ->
            GamesUi(
                id = results.id,
                name = results.name
            )
        }
}