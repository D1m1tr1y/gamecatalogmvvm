package com.example.gameappmvvm.main_page.model

import com.example.gameappmvvm.main_page.api.model.GamesResponse
import com.example.gameappmvvm.main_page.api.model.GenresDataResponse
import com.example.gameappmvvm.main_page.api.model.ResultsResponse

object GenresDataResponseConverter {
    fun fromNetwork (response : GenresDataResponse) : GenresData =
        GenresData(
            count = response.count,
            next = response.next,
            previous = response.previous,
            results = fromNetworkResults(response.results)
        )
    private fun fromNetworkResults(response : List<ResultsResponse>) : List<Results> =
        response.map { result->
            Results(
                id = result.id,
                name = result.name,
                slug = result.slug,
                gamesCount = result.gamesCount,
                image = result.image,
                games = fromNetworkGames(result.games)
            )
        }
    private fun fromNetworkGames(response: List<GamesResponse>) : List<Games> =
        response.map { result ->
            Games(
                id = result.id,
                name = result.name,
                slug = result.slug,
                added = result.added
            )
        }
}