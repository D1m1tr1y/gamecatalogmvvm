package com.example.gameappmvvm.main_page.ui.adapter_genres_recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.gameappmvvm.databinding.ItemGenresRecyclerviewBinding
import com.example.gameappmvvm.main_page.ui.model.ResultsUi

class GenresViewHolder(
    private val binding: ItemGenresRecyclerviewBinding
) : RecyclerView.ViewHolder(binding.root) {
    constructor(
        parent: ViewGroup
    ) : this(
        ItemGenresRecyclerviewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    )

    fun onBind(genreData: ResultsUi) {
        with(binding) {
            image.load(genreData.image)
            genreName.text = genreData.name
        }

    }

}