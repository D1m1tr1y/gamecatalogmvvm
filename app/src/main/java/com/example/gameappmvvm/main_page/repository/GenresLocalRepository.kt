package com.example.gameappmvvm.main_page.repository

import com.example.gameappmvvm.main_page.db.dao.GenresDao
import com.example.gameappmvvm.main_page.db.model.GenresEntityUiConverter
import com.example.gameappmvvm.main_page.ui.model.ResultsUi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class GenresLocalRepository(
    private val dao: GenresDao
) : LocalRepository {
    override suspend fun upsertCharacters(data: List<ResultsUi>) {
        val response = GenresEntityUiConverter.toDatabase(data)
    }

    override suspend fun getCharacters(): Flow<List<ResultsUi>> {
        return dao.getCharacters().map { list ->
            GenresEntityUiConverter.fromDatabase(list)
        }
    }
}