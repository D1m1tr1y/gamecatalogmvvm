package com.example.gameappmvvm.main_page.di

import com.example.gameappmvvm.common.di.InjectionModule
import com.example.gameappmvvm.db.RoomDataBase
import com.example.gameappmvvm.main_page.api.GenresApi
import com.example.gameappmvvm.main_page.interactor.MainPageInteractor
import com.example.gameappmvvm.main_page.repository.GenresLocalRepository
import com.example.gameappmvvm.main_page.repository.GenresRemoteRepository
import com.example.gameappmvvm.main_page.repository.LocalRepository
import com.example.gameappmvvm.main_page.repository.RemoteRepository
import com.example.gameappmvvm.main_page.ui.MainPageViewModel
import org.koin.androidx.viewmodel.dsl.viewModelOf
import org.koin.core.module.dsl.factoryOf
import org.koin.core.module.dsl.singleOf
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit

object MainPageModule : InjectionModule {
    override fun create() = module{
        single{get<Retrofit>().create(GenresApi::class.java)}
        single { get<RoomDataBase>().genresDao }
        single <LocalRepository>{ GenresLocalRepository(get()) }
        singleOf(::GenresLocalRepository) bind LocalRepository::class
        single<RemoteRepository>{ GenresRemoteRepository(get()) }
        singleOf(::GenresRemoteRepository) bind RemoteRepository::class
        factoryOf(::MainPageInteractor)
        viewModelOf(::MainPageViewModel)
    }


}