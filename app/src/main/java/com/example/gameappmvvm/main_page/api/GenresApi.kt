package com.example.gameappmvvm.main_page.api

import com.example.gameappmvvm.main_page.api.model.GenresDataResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface GenresApi {
    @GET("genres")
    suspend fun getData(
        @Query("key")key : String
    ): GenresDataResponse
}