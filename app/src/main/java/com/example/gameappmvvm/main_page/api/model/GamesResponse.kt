package com.example.gameappmvvm.main_page.api.model

import com.google.gson.annotations.SerializedName

data class GamesResponse(
    @SerializedName("id")
    val id : Int,
    @SerializedName("name")
    val name : String,
    @SerializedName("slug")
    val slug : String,
    @SerializedName("added")
    val added : Int
)
