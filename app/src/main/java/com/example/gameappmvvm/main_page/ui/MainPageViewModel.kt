package com.example.gameappmvvm.main_page.ui

import com.example.gameappmvvm.common.BaseViewModel
import com.example.gameappmvvm.main_page.interactor.MainPageInteractor
import com.example.gameappmvvm.main_page.ui.model.ResultsUi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import timber.log.Timber
import kotlin.coroutines.cancellation.CancellationException

class MainPageViewModel(
    private val interactor: MainPageInteractor
) : BaseViewModel() {
    private val _genresData = MutableStateFlow<List<ResultsUi>>(emptyList())
    val genresData = _genresData.asStateFlow()
    fun getGenres() {
        launch {
            _genresData.tryEmit(interactor.getGenres())
        }
    }

    fun upsertGenres() {
        launch {
            try {
                interactor.upsertGenres()

            } catch (e: CancellationException) {
                Timber.e(e.message)
            } catch (t: Throwable) {
                Timber.e(t.message)
            }
        }
    }
}
