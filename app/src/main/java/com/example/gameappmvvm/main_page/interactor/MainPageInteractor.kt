package com.example.gameappmvvm.main_page.interactor

import com.example.gameappmvvm.main_page.repository.GenresLocalRepository
import com.example.gameappmvvm.main_page.repository.GenresRemoteRepository
import com.example.gameappmvvm.main_page.ui.model.GenresUiConverter
import com.example.gameappmvvm.main_page.ui.model.ResultsUi

class MainPageInteractor(
    private val remoteRepository: GenresRemoteRepository,
    private val localRepository: GenresLocalRepository
) {
    suspend fun upsertGenres() {
        val data = GenresUiConverter.fromNetwork(
            remoteRepository.getData()
        )
        localRepository.upsertCharacters(
            data
        )
    }
    suspend fun getGenres(): List<ResultsUi>{
        var data = emptyList<ResultsUi>()
        localRepository.getCharacters().collect{
            data = it
        }
        return data
    }
}