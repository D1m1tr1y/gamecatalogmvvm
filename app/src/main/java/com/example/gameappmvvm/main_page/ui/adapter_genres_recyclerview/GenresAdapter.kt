package com.example.gameappmvvm.main_page.ui.adapter_genres_recyclerview

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.gameappmvvm.R
import com.example.gameappmvvm.main_page.ui.model.ResultsUi
import timber.log.Timber

class GenresAdapter : RecyclerView.Adapter<GenresViewHolder>() {
    private val genresData = mutableListOf<ResultsUi>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenresViewHolder {
        LayoutInflater.from(parent.context).inflate(R.layout.item_genres_recyclerview, parent, false)
        return GenresViewHolder(parent)
    }

    override fun getItemCount() = genresData.size


    override fun onBindViewHolder(holder: GenresViewHolder, position: Int) {
        val listItem = genresData[position]

        holder.onBind(listItem)
    }
    @SuppressLint("NotifyDataSetChanged")
    fun setData( values : List<ResultsUi>) {
        Timber.i("_______$values")
        genresData.clear()
        genresData.addAll(values)
        notifyDataSetChanged()
    }
}