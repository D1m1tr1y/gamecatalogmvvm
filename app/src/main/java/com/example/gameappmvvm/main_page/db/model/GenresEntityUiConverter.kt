package com.example.gameappmvvm.main_page.db.model

import com.example.gameappmvvm.main_page.ui.model.GamesUi
import com.example.gameappmvvm.main_page.ui.model.ResultsUi

object GenresEntityUiConverter {
    fun toDatabase(response: List<ResultsUi>): List<ResultsEntity> =
        response.map { result ->
            ResultsEntity(
                id = result.id,
                name = result.name,
                gamesCount = result.gamesCount,
                image = result.image,
                games = toDatabaseGames(result.games)
            )
        }

    private fun toDatabaseGames(games: List<GamesUi>): List<GamesEntity> =
        games.map { result ->
            GamesEntity(
                id = result.id,
                name = result.name
            )
        }

    fun fromDatabase(response: List<ResultsEntity>): List<ResultsUi> =
        response.map { result ->
            ResultsUi(
                id = result.id,
                name = result.name,
                gamesCount = result.gamesCount,
                image = result.image,
                games = fromDatabaseGames(result.games)
            )
        }

    private fun fromDatabaseGames(games: List<GamesEntity>): List<GamesUi> =
        games.map { result ->
            GamesUi(
                id = result.id,
                name = result.name
            )
        }
}