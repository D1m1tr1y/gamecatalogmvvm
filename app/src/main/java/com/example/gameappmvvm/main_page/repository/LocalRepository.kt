package com.example.gameappmvvm.main_page.repository

import com.example.gameappmvvm.main_page.ui.model.ResultsUi
import kotlinx.coroutines.flow.Flow

interface LocalRepository {
    suspend fun upsertCharacters(data: List<ResultsUi>)
    suspend  fun getCharacters  (): Flow<List<ResultsUi>>
}