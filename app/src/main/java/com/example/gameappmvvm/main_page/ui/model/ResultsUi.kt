package com.example.gameappmvvm.main_page.ui.model

data class ResultsUi(
    val id : Int,
    val name : String,
    val gamesCount : Int,
    val image : String,
    val games : List<GamesUi>
)
