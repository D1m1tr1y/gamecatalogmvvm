package com.example.gameappmvvm.main_page.db.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ResultsEntity(
    @PrimaryKey
    val id : Int,
    val name : String,
    val gamesCount : Int,
    val image : String,
    val games : List<GamesEntity>
)
