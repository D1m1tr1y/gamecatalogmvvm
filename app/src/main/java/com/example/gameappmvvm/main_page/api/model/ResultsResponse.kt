package com.example.gameappmvvm.main_page.api.model

import com.google.gson.annotations.SerializedName

data class ResultsResponse(
    @SerializedName("id")
    val id : Int,
    @SerializedName("name")
    val name : String,
    @SerializedName("slug")
    val slug : String,
    @SerializedName("games_count")
    val gamesCount : Int,
    @SerializedName("image_background")
    val image : String,
    @SerializedName("games")
    val games : List<GamesResponse>
)
