package com.example.gameappmvvm.main_page.db.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Upsert
import com.example.gameappmvvm.main_page.db.model.ResultsEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface GenresDao {
    @Upsert
    suspend fun upsertCharacter( genres : List<ResultsEntity>)
    @Query("SELECT * FROM ResultsEntity ")
    fun getCharacters() : Flow<List<ResultsEntity>>
}