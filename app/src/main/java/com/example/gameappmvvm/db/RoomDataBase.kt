package com.example.gameappmvvm.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.gameappmvvm.main_page.db.dao.GenresDao
import com.example.gameappmvvm.main_page.db.model.ResultsEntity

@Database(
    entities = [ResultsEntity::class],
    version = 1
)
abstract class RoomDataBase : RoomDatabase(){
    abstract val genresDao : GenresDao
}